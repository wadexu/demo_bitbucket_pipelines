package com.wadeshop.tutorial;

import java.time.LocalDateTime;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;

public class App {
	private HttpServer listen;

	public static void main(String[] args) {
		new App().run();

	}

	public void run() {
		listen = Vertx.vertx().createHttpServer()
				.requestHandler(req -> req.response().putHeader("content-type", "application/json").end("{\"message\":\"" + LocalDateTime.now() + "\"}")).listen(8080, handler -> {
					if (handler.succeeded()) {
						System.out.println("server is running: http://localhost:8080/");
					} else {
						System.err.println("server startup failed trying to listen on port 8080");
					}
				});
	}

	public void shutdown() {
		if (listen != null)
			listen.close();
	}

}
