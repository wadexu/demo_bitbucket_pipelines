package integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.wadeshop.tutorial.App;

import io.restassured.http.ContentType;

public class AppIntegrationTest {
	App app = new App();

	@Before
	public void setup() {
		app.run();
	}

	@After
	public void teardown() {
		app.shutdown();
	}

	@Test
	public void shouldReturnValidDate() throws Exception {
		given().contentType(ContentType.JSON).when().get("http://localhost:8080/").then().body("message", notNullValue());
	}
}
